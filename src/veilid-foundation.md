# About the Veilid Foundation

The Veilid name and brand are owned by the Veilid Foundation, a nonprofit that is dedicated to making sure that apps and networks built using the Veilid framework match the ideals of this project. We will have a public grading system on public projects using Veilid upon things like accessibility, transparency (open source), and non monetization. We will speak out publicly about any immoral uses of the framework, and will use the Veilid name to promote and support those who stick with the ideals we present. The foundation board is made up of:

* Christien “dildog” Rioux
* Katelyn “medus4” Bowden
* Paul “Gibson” Miller

The Foundation exists to support the entire project and provide assistance to those building projects within the core ideals spelled out previously, along with making sure that anything built using the Veilid community is legal, safe, and accessible to all. The Veilid Foundation does not support or encourage anyone using the framework for illegal purposes.

The Veilid Foundation, Inc. is a 501(c)3 corporation chartered with the state of Virginia.

We exist to develop, distribute, and maintain a privacy focused communication platform and protocol for the purposes of defending human and civil rights.

We can receive postal mail at:

Veilid Foundation, Inc.
P.O. Box 1917
Leesburg, VA 20177

Donations (tax-deductible in United States) can be made via Stripe, our payment processor, at <https://veilid.org/donate/>.

For more information, please visit <https://veilid.org/about-us/>.
