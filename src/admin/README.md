# Veilid Headless Nodes

All Veilid applications are nodes.  Mobile apps, web apps, native apps, all of them are nodes, and move traffic. 

Not all Veilid nodes are applications. Headless nodes are nodes without an application.  Their sole purpose is to move traffic, and hosting one is purely a Nice Thing To Do - an eventual goal is that the network will survive with only apps running if it has to.  The headless nodes provide a backbone of sorts to the network. In this Admin section, we will cover the installation, maintenance, and use of Veilid headless nodes. 

Installation can be from a repo, or compiled from source.
* [Installation from source.](source.md)
* [Installation from a repo.](repo.md)

Setup and configuration can take several forms. Aside from newtwork configuration, bootstrap installs have setup options.  It is also possible to setup an isolated network.
* [Configuration steps.](config.md)
* [Bootstrap server setup.](bootstrap.md)
* [Isolated network setup.](isolated.md)

Finally, the headless server does have a user interface for maintenance and observation.
* [Command Line Interface.](cli.md)
