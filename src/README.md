# Introduction

Veilid is the networking platform and protocol created, implemented, and maintained by the Veilid Foundation.

We exist to develop, distribute, and maintain a privacy focused communication platform and protocol for the purposes of defending human and civil rights.

        "Fight for the things you care about, but do it in a way that will
         lead others to join you." -Ruth Bader Ginsburg
