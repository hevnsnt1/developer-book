# Instantiating Veilid

Veilid starts up with the developer instantiating a new VeilidAPI object by using either `api_startup` or `api_startup_json`. This VeilidAPI object is a singleton, and cannot be instantiated again while it is running. (Once the VeilidAPI is shut down, it can be reinstantiated.)

In both `api_startup` and `api_startup_json`, the developer is expected to provide a callback function which is called on Veilid state updates, such as private route death, application messages, and routing table state changes.

The difference between them is that `api_startup` expects a second callback function to get the actual instance configuration, where `api_startup_json` expects a JSON object containing the configuration.
